#tmeter
tmeter是由tsung和jmeter组合而成，用于压力测试，其中tsung负责产生压力并生成测试报告，jmeter负责收集server主机上的相关数据（如：cpu，io，mem，network等），jmeter需要安装PerfMon插件，并在被测server上启动嗅探器收集数据，回传给jmeter。

[tsung使用帮助](http://my.oschina.net/enyo/blog/164057)

[jmeter使用帮助](http://my.oschina.net/enyo/blog/164753)

通过命令行启动：

	[root@localhost ma]# ./tsung_jmeter.sh -h
	-f | --testFile: tsung test file xml,default ./tsung_test.xml
	-u | --user: user number per second, default 1
	-x | --maxuser: max user number, default 8000
	-d | --duration: times used to generate user,default 60 s
	-t | --thinktime: the inteval time between two request,default 1 s
	-l | --loop: Each user's request number,default 60
	-s | --server: play server,default 192.168.7.116
	-p | --port: play server http port,default 9095
	-a | --api: api, default /monitor.jsp
	-m | --method: POST/GET,default GET
	-c | --contents: POST request body
	-h | --help: print this help

例，测试`192.168.7.116`机器上的`9095`端口，`/api/login`接口，方法是`POST`，参数是`a=b`：

	[root@localhost ma]# ./tsung_jmeter.sh -s 192.168.7.116 -p 9095 -a /api/login -m POST -c "a=b"

测试完成后会把tsung和jmeter的测试结果转移到apache和htdocs下，然后可以通过浏览器查看所有的测试记录
